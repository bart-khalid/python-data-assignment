# Python Data Assignment 

## Requirements
- Python 3.7 or higher.
#### - Install pipenv on your global python setup
```Python
    pip install pipenv 
```
Or follow [documentation](https://pipenv.pypa.io/en/latest/install/) to install it properly on your system
#### - Install requirements
```sh
    cd python-data-assignement
```
```Python
    pipenv install
```
```Python
    pipenv shell
```
#### - Start the application
```sh
    sh run.sh
```
- API : http://localhost:5000
- Streamlit Dashboard : http://localhost:8000

P.S You can check the log files for any improbable issues with your execution.

### 2 - Exploratory Data Analysis
In this section , I explored that data through various charts and plots.


![](./output/plots/screenEDA.png)

![](./output/plots/screenEDA2.png)



### 3 - Training 
In this section, I implemented other models in order to `beat` the baseline model.

- As first solution to increase the f1 score I changed the random_state in the phase of splitting data to 6 instead of 0.

![](./output/plots/ScreenDecision_tree.png)

- for the second solution I implemented 3 other models and they gave good results 

-- Random Forest

![](./output/plots/screenRandomForest.png)

-- KNN

![](./output/plots/screenKnn.png)

-- SVM

![](./output/plots/screenSvm.png)


The model that gives a higher `F1 score` is Random Forest.


Choose the `name` of the model and whether you want to `serialize` it.

![](./output/plots/chooseModel.png)




Once done, you will be able to see the F1 score as well as the confusion matrix.

P.S: If you chose the `save option` at the beginning of the training, you will be able to see the serialized model under `output/models/model_name`




### 4 - Inference

`Inference` is just a fancy word to say `prediction`.


P.S The original dataset has `29` features. But for the sake of simplification, we froze most of the features and only left `4` (`feature11`, `feature13`, `feature15`, `amount`) for you to tinker with.

The first example shows a configuration that renders a non fraudulent prediction:

![](./input/static/inference_clear.png)

The second example shows a configuration that renders a fraudulent prediction:

![](./input/static/inference_fraudulent.png)

