import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA


from src.constants import AGGREGATOR_MODEL_PATH, DATASET_PATH, CM_PLOT_PATH

class Insights:

    
    
    def __init__(self,dataframe) :
        self.df = dataframe
        self.head = self.df.head()
        self.tail = self.df.tail()
        self.desc = self.df.describe()

    def setAtrributes(self, df):
        self.df = df
        self.head = df.head()
        self.tail = df.tail()
        self.desc = df.describe()

    def getDistributionInfo(self) :
        dataSize = len(self.df)
        numClassSec = len(self.df[self.df.Class == 0])
        numClassfraud = len(self.df[self.df.Class == 1])
        fraud_percentage = round(numClassfraud/numClassSec*100, 2)
        return dataSize,numClassSec,numClassfraud,fraud_percentage

    def deleteDuplicatesObservations(self) :
        num = self.df[self.df.duplicated()].shape
        df = self.df[~self.df.duplicated()]
        self.setAtrributes(df)
        return num

    def deleteColumns(self, names):
        try :
            for i in names:
                df=self.df.drop(i, axis=1)
                self.df = df
            self.setAtrributes(df)
            return names
        except Exception as e:
                x='Failed to delete Column(s) '
                return x,e


    ### charts
    def barPlot(self):
        data = self.getDistributionInfo()[1:3]
        chart_data = pd.DataFrame(data, index=["Normal","Fraud"])
        return chart_data

    def plotPieChart(self):
        labels = 'Normal','Fraud' 
        sizes = self.getDistributionInfo()[1:3]
        explode = (0, 1) 
        fig1, ax1 = plt.subplots()
        fig1.patch.set_facecolor("none")
        ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',shadow=False, startangle=90, textprops={'color':'w'})
        ax1.axis('equal') 
        return fig1


    ### PCA reduction
    # scale data
    def scaleData(self):
        x = self.df.drop("Class",axis=1)
        scaler = StandardScaler()
        x_scaled = scaler.fit_transform(x)
        return x_scaled
    # 
    def applyPca(self):
        pca = PCA()
        pca.fit_transform(self.scaleData())
        cumulative_sum = np.cumsum(pca.explained_variance_ratio_)*100
        numComp = [n for n in range(len(cumulative_sum))]
        return cumulative_sum, numComp
    # plot 
    def plotCumPca(self):
        cum_sum, comp = self.applyPca()
        fig = plt.figure(figsize=(7,5))
        ax = fig.add_subplot(111)

        ax.spines['bottom'].set_color('red')
        ax.spines['left'].set_color('red')
        ax.xaxis.label.set_color('white')
        ax.yaxis.label.set_color('white')
        ax.tick_params(axis='x', colors='white')
        ax.tick_params(axis='y', colors='white')

        ax.set_facecolor('none')
        fig.patch.set_facecolor("none")

        ax.set_title('ax1 title', color="white")
        plt.plot(comp, cum_sum, marker='.')
        plt.xlabel('PCA Components')
        plt.ylabel('Cumulative Explained Variance (%)')
        plt.title('PCA')
        plt.show()
        return fig,cum_sum