from sklearn.neighbors import KNeighborsClassifier

from src.models.base_model import BaseModel


class KNNModel(BaseModel):
    def __init__(self, n_neighbors=4):
        self.n_neighbors = n_neighbors
        super().__init__(
            model=KNeighborsClassifier(n_neighbors = self.n_neighbors)
        )

    def bestKvalue(self,x_train, y_train):
        scoreList = []
        for i in range(1,20):
            knn2 = KNeighborsClassifier(n_neighbors = i)  
            knn2.fit(x_train.T, y_train.T)
            scoreList.append(knn2.score(x_train, y_train))
    
        acc = max(scoreList)*100
        bestK = scoreList.index(acc)
        return bestK