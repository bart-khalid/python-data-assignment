from sklearn.svm import SVC

from src.models.base_model import BaseModel


class SVCModel(BaseModel):
    def __init__(self, kernel='rbf'):
        self.kernel = kernel
        super().__init__(
            model=SVC(kernel = self.kernel)
        )
