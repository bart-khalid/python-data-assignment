import time
import streamlit as st
import requests
from PIL import Image
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

# import this class to extract insights
from src.EDA.insights import Insights


from src.constants import AGGREGATOR_MODEL_PATH, DATASET_PATH, CM_PLOT_PATH
from src.constants import INFERENCE_EXAMPLE, CM_PLOT_PATH
from src.training.train_pipeline import TrainingPipeline


st.title("Card Fraud Detection Dashboard")
st.sidebar.title("Data Themes")

sidebar_options = st.sidebar.selectbox(
    "Options",
    ("EDA", "Training", "Inference")
)

if sidebar_options == "EDA":
    st.header("Exploratory Data Analysis")
    with st.spinner('Exploratory Data ANalysis, please wait...'):
        time.sleep(2)
        try :
            # read the dataset
            dataframe = pd.read_csv(DATASET_PATH)
            # instanciate my class
            ins = Insights(dataframe)
            ### inspecting dataset
            st.subheader('The Head of the dataset to get an idea about our features') 
            st.dataframe(ins.head)
            st.subheader('The Tail') 
            st.dataframe(ins.tail)
            st.subheader('The section below shows a description of our dataset')
            st.dataframe(ins.desc)
            st.subheader("Checking how many nan values exist for eatch column")
            st.dataframe(ins.df.isna().sum())
            # conclusion
            st.text("no nan values found")

            # Duplicates Observations
            st.subheader("Search and delete duplicates")
            st.write("Number of duplicates found", ins.deleteDuplicatesObservations())
            st.write("New size of dataset : ", len(ins.df))

            ### information about distribution of observations
            st.info("Information about the distribution of the observations")
            dataSize,numClassSec,numClassfraud,fraud_percentage = ins.getDistributionInfo()
            st.write('Total number of Transactions :', dataSize)
            st.write('Total number of Normal Transactions :', numClassSec)
            st.write('Total number of Fraud Transactions :', numClassfraud)
            st.write('Percentage of Fraud Transactions :', fraud_percentage)

            ### target variable
            st.subheader("Checking if the target attribute contains only 2 distinct values (1 and 0)")
            st.dataframe(ins.df["Class"].value_counts())
            # barplot
            st.subheader("Target variable count")
            st.bar_chart(ins.barPlot())
            # plot a pie chart
            st.pyplot(ins.plotPieChart())
            # conclusion
            st.write("From the above piechart and barplot, there is a clear domination of the class 0 (Normal) over the class 1 (Fraud) ")
            
            ### correlation
            # Apply PCA 
            st.header("Using 'PCA' to reduce dimensions")
            st.pyplot(ins.plotCumPca()[0])
            # conclusion
            st.write("from the above chart, we notice that just two features 'could' be removed and allow the model to still have"
                    " '99%' of the explainability."
                    " however in this dataset the features appears to be already transformed by PCA and so we could potentially just remove 2 features."
                    "'Time' and 'Amount'.")
            # drop variables
            st.subheader("drop unuseful variables")
            st.write("the only variables that can be dropped, are 'Time' and 'Amount' because they don't significantly contribute in the classification")
            dropedColumns = ins.deleteColumns(["Amount","Time"])
            st.write("the following columns are succesfully dropped", dropedColumns)
            st.subheader("new Head")
            st.dataframe(ins.head)  

        except Exception as e:
                st.error('Failed to load Data Analysis!')
                st.exception(e)


elif sidebar_options == "Training":
    st.header("Model Training")
    option = st.selectbox(
     'Select a model to train ',
     ('Decision Tree', 'Random Forest', 'SVM', 'KNN'))
    st.write('You selected:', option)
    serialize = st.checkbox('Save model')
    train = st.button('Train Model')

    if train:
        with st.spinner('Training model, please wait...'):
            time.sleep(1)
            try:
                tp = TrainingPipeline()
                tp.train(serialize=serialize, model_name=option)
                tp.render_confusion_matrix(plot_name=option)
                accuracy, f1 = tp.get_model_perfomance()
                col1, col2 = st.columns(2)

                col1.metric(label="Accuracy score", value=str(round(accuracy, 4)))
                col2.metric(label="F1 score", value=str(round(f1, 4)))

                st.image(Image.open(CM_PLOT_PATH))

            except Exception as e:
                st.error('Failed to train model!')
                st.exception(e)


else:
    st.header("Fraud Inference")
    feature_11 = st.slider('Transaction Feature 11', -10.0, 10.0, step=0.001, value=-4.075)
    feature_13 = st.slider('Transaction Feature 13', -10.0, 10.0, step=0.001, value=0.963)
    feature_15 = st.slider('Transaction Feature 15', -10.0, 10.0, step=0.001, value=2.630)
    #amount = st.number_input('Transaction Amount', value=1000, min_value=0, max_value=int(1e10), step=100)
    infer = st.button('Run Fraud Inference')

    INFERENCE_EXAMPLE[10] = feature_11
    INFERENCE_EXAMPLE[12] = feature_13
    INFERENCE_EXAMPLE[15] = feature_15
    #INFERENCE_EXAMPLE[28] = amount

    if infer:
        with st.spinner('Running inference...'):
            time.sleep(1)
            try:
                result = requests.post(
                    'http://localhost:5000/api/inference',
                    json=INFERENCE_EXAMPLE
                )
                if int(int(result.text) == 1):
                    st.success('Done!')
                    st.metric(label="Status", value="Transaction: Fraudulent")
                else:
                    st.success('Done!')
                    st.metric(label="Status", value="Transaction: Clear")
            except Exception as e:
                st.error('Failed to call Inference API!')
                st.exception(e)
